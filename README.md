Aim: 
Create 3 Virtual machines, one which runs ubutu, one which runs aws-linux and lastly one which runs haproxy.

The script will read the IPs for each VM and download all necessary software (i.e Apache, NGIX).

The webservers should both be accessible from haproxy's public IP address. 