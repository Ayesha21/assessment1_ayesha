#!/bin/bash
if (( $# > 0 ))
then
  hostname=$1
else
  echo "Supply HAproxy IP address" 1>&2
  exit 1
fi

ssh -o StrictHostKeyChecking=no -i ~/.ssh/AyeshaShamsiKey.pem ubuntu@$hostname ' 
sudo add-apt-repository ppa:vbernat/haproxy-1.8
sudo apt-get update
sudo apt-get install haproxy

sudo sh -c "echo \"global 
    log 127.0.0.1 local0 notice
    maxconn 200
    user haproxy
    group haproxy

defaults
    log      global
    mode     http
    option   httplog
    option   dontlognull
    retires  3
    option   redispatch
    timeout  connect   5000
    timeout  client    10000
    timeout  server    10000
    

frontend simple_load_balancer
    bind *:80
    mode http
    option httpclose
    option forwardfor
    default_backend simple_servers

backend simple_servers
    balance roundrobin
    server lamp1 172.31.3.108:80
    server lamp2 172.31.14.222:80
\" > /etc/haproxy/haproxy. cfg"

sudo service haproxy restart
 

# sudo ufw allow from any to any port 80 proto tcp
'